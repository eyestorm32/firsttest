﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace TestJson
{
    //to create object for Person
    public class Movie
    {
        public string Name { get; set; }
        public int Year { get; set; }

        public Movie (string name, int year)
        {
            Name = name;
            Year = year;
        }
    }
    //Main object to focus on
    public class Person
    {
        public string Name { get; set; }
        public string Email { get; set; }
        int weight;
        [JsonProperty("Mass")]
        public int Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        public Movie HobbyMovie { get; set; }
        public override string ToString()
        {
            return $"Name: {Name},Email: {Email},Weight {Weight}, HobbyMovie : {HobbyMovie.Name}";
            //return $"Name: {Name},Email: {Email},Weight {Weight}";
        }
    }
    //Container (List) to contain person 
    public class Persons
    {
        public List<Person> aListOfPerson = new List<Person>();

        //public Persons(Person[] aArray)
        //{
        //    aListOfPerson.AddRange(aArray);
        //}
        public override string ToString()
        {
            foreach (Person p in aListOfPerson)
                Console.WriteLine(p.ToString());
            return base.ToString();
        }
        //Contructor for Class Persons List
        public Persons(Person[] aArray)
        {
            aListOfPerson.AddRange(aArray);
        }
        //Default Contructor even without declar
        public Persons()
        {

        }
    }
    //Container to contain list
    public class ContainerPersons
    {
        public Persons personslist { get; set; }

        public override string ToString()
        {
            foreach (Person p in personslist.aListOfPerson)
                Console.WriteLine(p.Name);
            return base.ToString();
        }
    }


    //Another Example to observe
    #region Example
    public class positionsobj
    {
        public positionlist positions { get; set; }
    }

    public class positionlist
    {
        public List<position> position { get; set; }
    }

    public class position
    {
        public float cost_basis { get; set; }
        public DateTime date_acquired { get; set; }
        public int id { get; set; }
        public decimal quantity { get; set; }
        public string symbol { get; set; }
    }
    #endregion

    class Program
    {
        public static void TestWrite(Persons persons)
        {
            string fileName = @"C:\TestFolder\testListofPerson.json";
            var serializer = new JsonSerializer { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto };
            using (TextWriter writer = File.CreateText(fileName))
            {
                serializer.Serialize(writer, persons);
            }

            string jsonoutput = JsonConvert.SerializeObject(persons, Formatting.Indented);
        }

        public static void TestReadToConsole()
        {
            //Person personFromReading;
            //List<Person>
            //string fileName = @"C:\TestFolder\testListofPerson.json";
            //var serializer = new JsonSerializer { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto };

            //using (TextReader reader = File.OpenText(fileName))
            //{
            //    //var personFromReading = serializer.Deserialize(reader, typeof(Person)) as Person;

            //    //Console.WriteLine(personFromReading.GetType());  //OutPut
            //    //Console.WriteLine(personFromReading); //OutPut
            //    //var aaa = serializer.Deserialize(reader, Persons) as Person;

            //    
            //}
            

            ////string json = @"['Starcraft','Halo','Legend of Zelda']";

            //List<Person> videogames = JsonConvert.DeserializeObject<List<Person>>(personFromReading);

            //Console.WriteLine(string.Join(", ", videogames.ToArray()));
        }


        static void Main(string[] args)
        {
            //Example for observe
            #region example
            //var responsebody = "{\"positions\":{\"position\":[{\"cost\":102.20000,\"date_acquired\":\"2013-11-18T19:54:13.730Z\",\"id\":192,\"quantity\":2.00000,\"symbol\":\"C\"},{\"cost\":121.50990,\"date_acquired\":\"2013-11-20T17:43:41.737Z\",\"id\":199,\"quantity\":1.00000,\"symbol\":\"TSLA\"}]}}";
            //positionsobj position = JsonConvert.DeserializeObject<positionsobj>(responsebody);

            //foreach (position item in position.positions.position)
            //{
            //    Console.WriteLine("Test : {0}", item.id);
            //}

            #endregion
            //*************
            //My own example to research
            //Create object for Class: Person - For testing for Object only
            Person person1 = new Person { Email = "schroedinger@mit.org", Name = "Schrödinger", Weight = 70, HobbyMovie = new Movie("Titanic", 1997) };
            Person person2 = new Person { Email = "test@mit.org", Name = "tesstt", Weight = 70, HobbyMovie = new Movie("Drakula", 1990) };

            //Create a normal List which contain object - For testing for List<T>
            List<Person> aNormalList = new List<Person> { person1, person2 };
            Console.WriteLine(aNormalList);

            //Create object for Container(List):
            Person[] aPersonArray = new Person[] { person1, person2 };
            Persons aContainerforList = new Persons(aPersonArray);

            ////Create object for Class Wrapper contain Container(List) //For higher level of container, but not at this example, 
            /////Need check syntas of Json file to determine how many level of container to deserialize
            //ContainerPersons aWrapperForList = new ContainerPersons()

            //*********

            //USAGE JSON SCENARIO:
            //Actually we create a json file (*.json), but we can create others files but json with other name extenxion (*.test, *.drw...)
            //only need to use syntac json for file.

            string placeToSave = @"C:\TestFolder\testListofPerson.test";
            //1st CASE: FOR OBJECT ONLY
            //1.1
            //Serialize Object
            //1.1.1 Methode 1:
            var serializer0 = new JsonSerializer {};
            using (TextWriter writer = File.CreateText(placeToSave))
            {
                serializer0.Serialize(writer, person1);
            };
            //Already replaced " to '
            string result0WillLookLike = @"{'Name':'Schrödinger','Email':'schroedinger@mit.org','Mass':70,'HobbyMovie':{'Name':'Titanic','Year':1997}}";


            //Formatting = Formatting.Indented        for The Identation in format of json file,  for a better review
            //TypeNameHandling = TypeNameHandling.Auto  : Very importance for the polymorph object, such as in our Exercise: 
            //object is based on not unique class,  but many class: Line, Circle, Polyline..and root is class Curve
            var serializer1 = new JsonSerializer { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto };
            using (TextWriter writer = File.CreateText(placeToSave))
            {
                serializer1.Serialize(writer, person1);
            };
            //Already replaced " to '
            string result1WillLookLike = @"{
                                            'Name': 'Schrödinger',
                                            'Email': 'schroedinger@mit.org',
                                            'Mass': 70,
                                            'HobbyMovie': {
                                            'Name': 'Titanic',
                                            'Year': 1997 }
                                           }";

            //Notice that, with object, there is no Class name on it, only { will displayed
            //1.1.2 Method 2:
            string ajson0 = JsonConvert.SerializeObject(person2);
            //Use File from System.IO of C#, not Newtonsoft.Json direct to file
            File.WriteAllText(placeToSave, ajson0);
            //or with Indented and TypeNamHandling
            string ajson1 = JsonConvert.SerializeObject(person2, Formatting.Indented);
            File.WriteAllText(placeToSave, ajson1);
            //or 
            string ajson2 = JsonConvert.SerializeObject(person2, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
            });
            File.WriteAllText(placeToSave, ajson2);
            //or 
            string ajson3 = JsonConvert.SerializeObject(person2, new JsonSerializerSettings
            {
                Formatting=Formatting.Indented,
                TypeNameHandling = TypeNameHandling.Objects,
            });
            File.WriteAllText(placeToSave, ajson2);

            //1.2
            //Deserialize Object
            //1.2.1 Method 1
            var serializer = new JsonSerializer { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto };
            using (TextReader reader = File.OpenText(placeToSave))
            {
                Person atest = serializer.Deserialize(reader, typeof(Person)) as Person;
                Console.WriteLine(atest);
            }
            //1.2.2 Method 2
            //Using File from System.IO of C#
            string aJsonString = File.ReadAllText(placeToSave);
            Person aTest = JsonConvert.DeserializeObject<Person>(aJsonString, new JsonSerializerSettings
            {
                //Formatting = Formatting.Indented, 
                TypeNameHandling = TypeNameHandling.Auto
            }) as Person; 
            Console.WriteLine(aTest);

            //2nd CASE: FOR A SIMPLE LIST <T> OR ARRAY 
            //2.1 Serialize a Collection: Exactly the same syntac with serialize a object,  just change a object and Type.
            //2.1.1 Method :

            string aJsonAfterConvert = JsonConvert.SerializeObject(aNormalList, Formatting.Indented); //Doesnt work if there is Array Contructor in Class Contain List
            string aJsonAfterConvert2 = JsonConvert.SerializeObject(aPersonArray, Formatting.Indented);

            //Then write it to File:
            File.WriteAllText(placeToSave, aJsonAfterConvert);

            //2.2 Desialize a Collection: 
            //2.2.1 Method 1:
            var ser = new JsonSerializer { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto };
            using (TextReader reader = File.OpenText(placeToSave))
            {
                List<Person> atest = ser.Deserialize(reader, typeof(List<Person>)) as List<Person>; //Work case only if there is no Array Contructor in List
                //or Array:
                //Person[] atest2 = ser.Deserialize(reader, typeof(Person[])) as Person[]; //Work case
                Console.WriteLine(atest);
            }

            //2.2.2 Method 2:
            string aJsonAfterReading = File.ReadAllText(placeToSave);
            Person[] aTest2 = JsonConvert.DeserializeObject<Person[]>(aJsonAfterReading, new JsonSerializerSettings
            {
                //Formatting = Formatting.Indented, 
                TypeNameHandling = TypeNameHandling.Auto
            }) as Person[];

            //or List:
            List<Person> aTest3 = JsonConvert.DeserializeObject<List<Person>>(aJsonAfterReading, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            }) as List<Person>;

            //3rd CASE: FOR A CONTAINER CLASS (WITH CONTAIN LIST OR ARRAY IN CONTRUCTOR INSIDE OF IT)
            //IN THIS CASE, WE HAVE TO CHECK OF CONTRUCTOR OF CONTAINER LIST IS ACTUAL LIST OR ARRAY. 
            //IF CONTAINER LIST HAS LIST CONTRUCTOR: (OR DECLARE NOTHING) SEE ABOVE EXAMPLE TO OBSERVE FOR DETAIL
            //IF CONTAINER LIST HAS ARRAY CONTRUCTOR (SUCH AS CLASS DRAWING IN OUR EXERCISES, SEE FOLLOWING):
            //3.1 SERIALIZE:
            // JUST AS SERIALIZE AN OBJECT AS USUAL
            //3.2 DESERIALIZE:
            //3.2.1 Syntac 1:
            var serial= new JsonSerializer { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto };
            using (TextReader reader = File.OpenText(placeToSave))
            {
                Person[] atest = serial.Deserialize(reader, typeof(Person[])) as Person[];
                aNormalList.AddRange(atest); //Add Element to our List.
            }
            //3.2.2 Syntac 2:
            string aJsonString0 = File.ReadAllText(placeToSave);
            Person[] aArrayAfter = JsonConvert.DeserializeObject<Person[]>(aJsonString0, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            }) as Person[]; 
            Console.WriteLine(aArrayAfter);
            aNormalList.AddRange(aArrayAfter);


            //4. NOTICE ABOUT SYNTAC IN JSON FILE:
            // the Number and Position of { and [ at beginning or at the end of json file, with helf of private _variable (list) will show the level of Container or Wrapper Class 
            //https://stackoverflow.com/questions/20192403/cannot-deserialize-json-result-to-custom-list-object-using-c-sharp/20192734

            Console.ReadKey();
        }
    }
}
